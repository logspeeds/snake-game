/* eslint-disable linebreak-style */
import React from "react";
import "./App.css";
import SnakeGame from "./components/snakegame";

function App() {
	return (
		<div className="App">
			<SnakeGame />
		</div>
	);
}

export default App;

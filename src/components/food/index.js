import React from "react";
import "./index.css";

const Food = (location) => {
	const style = {
		left: `${location.foodLocation[0]}%`,
		top: `${location.foodLocation[1]}%`
	};
	return (
		<div className="snake--food" style={style}></div>
	);
};

export default Food;
import React from "react";
import "./index.css";

const Snake = (snakeDots) => {
	return (
		<div>
			{snakeDots.snakeDots.map((dot , i)=>{
				const style = {
					left: `${dot[0]}%`,
					top: `${dot[1]}%`
				};
				return (
					<div className="snake--dot" key={i} style={style}></div>
				);
			})}
		</div>
	);
};

export default Snake;